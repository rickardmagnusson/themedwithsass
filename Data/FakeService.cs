﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace W360Shell
{
    public class FakeService
    {
        private List<User> _users;

        public List<User> CreateUsers()
        {
            return Enumerable.Range(1, 150).Select(index =>
            {
                return new User(index);

            }).ToList();
        }

        protected List<User> Users
        {
            get
            {
                if (_users == null)
                    _users = CreateUsers();
                return _users;
            }
        }

        public Task<IEnumerable<User>> GetUsersAsync(CancellationToken ct = default)
        {
            return Task.FromResult(Users.AsEnumerable());
        }

        public IEnumerable<User> GetUsers()
        {
            return Users;
        }
    }

    public class User { 

        public int Id {  get; set; }
        public string Name {  get; set; }
        public string Lastname {  get; set; }

        public string FirstName = @"Jan,Per,Bjørn,Ole,Lars,Kjell,Knut,Anne,Inger,Kari,Marit,Ingrid,Liv,Eva";
        public string LastNames = @"Hansen,Johansen,Olsen,Larsen,Magnusson,Nordby";

        public User(int id) 
        { 
            var rand = new Random();

            Id = id;
            Name = FirstName.Split(',')[rand.Next(0, 14)];
            Lastname = LastNames.Split(',')[rand.Next(0, 6)];
        }
    }
}
