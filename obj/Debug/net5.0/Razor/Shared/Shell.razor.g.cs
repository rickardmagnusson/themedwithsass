#pragma checksum "D:\WlCom\themedwithsass\Shared\Shell.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ef315879217aa239d673b4a1f20f15de078dae15"
// <auto-generated/>
#pragma warning disable 1591
namespace W360Shell.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\WlCom\themedwithsass\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\WlCom\themedwithsass\_Imports.razor"
using W360Shell;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\WlCom\themedwithsass\_Imports.razor"
using W360Shell.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\WlCom\themedwithsass\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\WlCom\themedwithsass\_Imports.razor"
using W360Shell.Pages.Parts;

#line default
#line hidden
#nullable disable
    public partial class Shell : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "section");
            __builder.AddAttribute(1, "class", "shell");
            __builder.AddAttribute(2, "id", "shell");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "errors");
            __builder.AddMarkupContent(5, "An error occurred: \r\n        ");
            __builder.OpenElement(6, "pre");
            __builder.AddAttribute(7, "id", "error-log");
#nullable restore
#line 7 "D:\WlCom\themedwithsass\Shared\Shell.razor"
__builder.AddContent(8, error);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 12 "D:\WlCom\themedwithsass\Shared\Shell.razor"
      

    [Parameter]
    public string error{ get; set; }

    protected override Task OnInitializedAsync()
    {
        this.exceptionNotificationService.OnException += HandleExceptions;
        
        return base.OnInitializedAsync();
    }

    private void HandleExceptions(object sender, string s)
    {
        error = s;
    }

    public void Dispose()
    {
        this.exceptionNotificationService.OnException -= HandleExceptions;
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ExceptionNotificationService exceptionNotificationService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JsInterop { get; set; }
    }
}
#pragma warning restore 1591
