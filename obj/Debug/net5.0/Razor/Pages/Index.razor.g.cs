#pragma checksum "D:\WlCom\themedwithsass\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "186b63f67026dca81aaae1fe0845dafd1b2372bb"
// <auto-generated/>
#pragma warning disable 1591
namespace W360Shell.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\WlCom\themedwithsass\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\WlCom\themedwithsass\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\WlCom\themedwithsass\_Imports.razor"
using W360Shell;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\WlCom\themedwithsass\_Imports.razor"
using W360Shell.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\WlCom\themedwithsass\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\WlCom\themedwithsass\_Imports.razor"
using W360Shell.Pages.Parts;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<W360Shell.Shared.PanelSplitter>(0);
            __builder.AddAttribute(1, "Left", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<W360Shell.Shared.Card>(2);
                __builder2.AddAttribute(3, "Title", "Orders");
                __builder2.AddAttribute(4, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.OpenComponent<W360Shell.Shared.Spinner>(5);
                    __builder3.AddAttribute(6, "Visible", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 7 "D:\WlCom\themedwithsass\Pages\Index.razor"
                               Visible

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(7, "\r\n            ");
                    __builder3.OpenComponent<DevExpress.Blazor.DxDataGrid<User>>(8);
                    __builder3.AddAttribute(9, "Data", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<User>>(
#nullable restore
#line 9 "D:\WlCom\themedwithsass\Pages\Index.razor"
                               Data

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(10, "CssClass", "´table-sm w-100");
                    __builder3.AddAttribute(11, "ShowGroupPanel", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 12 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                        true

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(12, "ShowFilterRow", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 13 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                       true

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(13, "PageSize", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 14 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                  30

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(14, "RowClick", (System.Action<DevExpress.Blazor.DataGridRowClickEventArgs<User>>)(
#nullable restore
#line 16 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                  OnViewGridRowClick

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(15, "DataNavigationMode", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.DataGridNavigationMode>(
#nullable restore
#line 17 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                            DataGridNavigationMode.Paging

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(16, "HtmlDataCellDecoration", (System.Action<DevExpress.Blazor.DataGridHtmlDataCellDecorationEventArgs<User>>)(
#nullable restore
#line 18 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                 OnHtmlDataCellDecoration

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(17, "Columns", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.OpenComponent<DevExpress.Blazor.DxToolbar>(18);
                        __builder4.AddAttribute(19, "ItemRenderStyleMode", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ToolbarRenderStyleMode>(
#nullable restore
#line 20 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                    ToolbarRenderStyleMode.Plain

#line default
#line hidden
#nullable disable
                        ));
                        __builder4.AddAttribute(20, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.OpenComponent<DevExpress.Blazor.DxToolbarItem>(21);
                            __builder5.AddAttribute(22, "RenderStyle", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ButtonRenderStyle>(
#nullable restore
#line 21 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                    ButtonRenderStyle.Light

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(23, "BeginGroup", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 21 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                                                         true

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(24, "IconCssClass", "fas fa-sync-alt");
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(25, "\r\n                        ");
                            __builder5.OpenComponent<DevExpress.Blazor.DxToolbarItem>(26);
                            __builder5.AddAttribute(27, "RenderStyle", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ButtonRenderStyle>(
#nullable restore
#line 22 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                    ButtonRenderStyle.Light

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(28, "IconCssClass", "fas fa-search");
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(29, "\r\n                        ");
                            __builder5.OpenComponent<DevExpress.Blazor.DxToolbarItem>(30);
                            __builder5.AddAttribute(31, "RenderStyle", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ButtonRenderStyle>(
#nullable restore
#line 23 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                    ButtonRenderStyle.Light

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(32, "BeginGroup", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 23 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                                                         true

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(33, "IconCssClass", "fas fa-plus");
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(34, "\r\n                        ");
                            __builder5.OpenComponent<DevExpress.Blazor.DxToolbarItem>(35);
                            __builder5.AddAttribute(36, "RenderStyle", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ButtonRenderStyle>(
#nullable restore
#line 24 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                    ButtonRenderStyle.Light

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(37, "IconCssClass", "fas fa-save");
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(38, "\r\n                        ");
                            __builder5.OpenComponent<DevExpress.Blazor.DxToolbarItem>(39);
                            __builder5.AddAttribute(40, "RenderStyle", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ButtonRenderStyle>(
#nullable restore
#line 25 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                    ButtonRenderStyle.Light

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(41, "IconCssClass", "fas fa-trash");
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(42, "\r\n                        ");
                            __builder5.OpenComponent<DevExpress.Blazor.DxDataGridColumnChooserToolbarItem>(43);
                            __builder5.AddAttribute(44, "CssClass", "btn-sm");
                            __builder5.AddAttribute(45, "BeginGroup", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 26 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                                                          true

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(46, "Text", "ColumnChooser");
                            __builder5.AddAttribute(47, "Alignment", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<DevExpress.Blazor.ToolbarItemAlignment>(
#nullable restore
#line 26 "D:\WlCom\themedwithsass\Pages\Index.razor"
                                                                                                                                ToolbarItemAlignment.Right

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.CloseComponent();
                        }
                        ));
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(48, "\r\n                    ");
                        __builder4.OpenComponent<DevExpress.Blazor.DxToolbar>(49);
                        __builder4.AddAttribute(50, "TitleTemplate", (Microsoft.AspNetCore.Components.RenderFragment<System.Object>)((context) => (__builder5) => {
                            __builder5.OpenComponent<W360Shell.Pages.Parts.PropertiesPanel>(51);
                            __builder5.CloseComponent();
                        }
                        ));
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(52, "\r\n                    ");
                        __builder4.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(53);
                        __builder4.AddAttribute(54, "Field", "RowIndex");
                        __builder4.AddAttribute(55, "Caption", "#");
                        __builder4.AddAttribute(56, "Width", "60px");
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(57, "\r\n                    ");
                        __builder4.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(58);
                        __builder4.AddAttribute(59, "Field", "Id");
                        __builder4.AddAttribute(60, "Width", "60px");
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(61, "\r\n                    ");
                        __builder4.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(62);
                        __builder4.AddAttribute(63, "Field", "Name");
                        __builder4.AddAttribute(64, "Width", "100px");
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(65, "\r\n                    ");
                        __builder4.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(66);
                        __builder4.AddAttribute(67, "Field", "Lastname");
                        __builder4.CloseComponent();
                    }
                    ));
                    __builder3.AddComponentReferenceCapture(68, (__value) => {
#nullable restore
#line 8 "D:\WlCom\themedwithsass\Pages\Index.razor"
                              Grid = (DevExpress.Blazor.DxDataGrid<User>)__value;

#line default
#line hidden
#nullable disable
                    }
                    );
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.AddAttribute(69, "Right", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<W360Shell.Shared.Card>(70);
                __builder2.AddAttribute(71, "Title", "Overview");
                __builder2.AddAttribute(72, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddMarkupContent(73, "\r\n            Details in overview\r\n            <br><br>\r\n            Test exception<br>\r\n            ");
                    __builder3.AddMarkupContent(74, "<a onclick=\"functionNotDefined()\" href=\"#\">Test the console shell</a>");
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 51 "D:\WlCom\themedwithsass\Pages\Index.razor"
       
    public bool Visible { get; set; } = true;
    IEnumerable<User> Data { get; set; }
    private DxDataGrid<User> Grid { get; set; }

    protected override void OnInitialized() {
        Visible = true;
        StateHasChanged(); //Invoke loader
        Data = FakeService.GetUsers().ToList();
        Visible = false;
        StateHasChanged(); //Invoke loader
    }

    protected async void OnViewGridRowClick(DataGridRowClickEventArgs<User> args)
    {
        Visible = true;
        StateHasChanged(); //Invoke loader

        Visible = false;
        StateHasChanged(); //Unload loader
    }

    protected void OnHtmlDataCellDecoration(DataGridHtmlDataCellDecorationEventArgs<User> cell)
    {
        if (cell.FieldName == "RowIndex")
            cell.AddRowIndex(Grid);

        if (cell.FieldName == "Name")
            cell.SetCellTextBold("Name");
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private FakeService FakeService { get; set; }
    }
}
#pragma warning restore 1591
