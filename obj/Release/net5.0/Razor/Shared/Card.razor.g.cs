#pragma checksum "C:\Users\rickard\Desktop\Shell\themedwithsass\Shared\Card.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4f98b720e4a3a4fc187eccf195c4330224a39d03"
// <auto-generated/>
#pragma warning disable 1591
namespace W360Shell.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using W360Shell;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using W360Shell.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\rickard\Desktop\Shell\themedwithsass\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
    public partial class Card : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "card");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "card-header");
            __builder.AddContent(4, 
#nullable restore
#line 3 "C:\Users\rickard\Desktop\Shell\themedwithsass\Shared\Card.razor"
     Title

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(5, "\r\n    ");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "card-body");
            __builder.AddContent(8, 
#nullable restore
#line 6 "C:\Users\rickard\Desktop\Shell\themedwithsass\Shared\Card.razor"
         ChildContent

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 10 "C:\Users\rickard\Desktop\Shell\themedwithsass\Shared\Card.razor"
      
    [Parameter] public string Title{ get; set; }
    [Parameter] public RenderFragment ChildContent{ get; set; }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
